package hangman;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * Based on https://web.archive.org/web/20090909075401if_/http://wortschatz.uni-leipzig.de/Papers/top10000de.txt,
 * converted to UTF-8.
 */
public class Words {

    public static String[] topGermanWords(int minLength) {
        try (var in = Words.class.getResourceAsStream("top10000de.txt");
             var reader = new BufferedReader(new InputStreamReader(in, UTF_8))) {
            return reader.lines()
                    .map(Words::normalize)
                    .distinct()
                    .filter(w -> w.matches("[A-Z]+") && w.length() >= minLength)
                    .toArray(String[]::new);
        } catch (IOException | NullPointerException e) {
            throw new AssertionError(e);
        }
    }

    private static String normalize(String word) {
        return word.toUpperCase()
                .replace("ß", "SS")
                .replace("Ä", "AE")
                .replace("Ö", "OE")
                .replace("Ü", "UE");
    }
}
